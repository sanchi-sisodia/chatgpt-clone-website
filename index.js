// Add functionality using JavaScript
// Assuming you have FontAwesome linked for the paper plane icon

// Functionality for sending a message
document.querySelector(".send-button").addEventListener("click", function () {
    const messageInput = document.querySelector(".message-input");
    const messageText = messageInput.value;

    if (messageText.trim() !== "") {
        const chatSection = document.querySelector(".chat-section");
        const newMessage = document.createElement("div");
        newMessage.classList.add("chat-message");
        newMessage.innerHTML = `
            <div class="message-icon"></div>
            <div class="message-text">${messageText}</div>
        `;
        chatSection.appendChild(newMessage);

        messageInput.value = "";
    }
});

// Functionality for clearing last chat on new chat box click
document.querySelector(".new-chat-box").addEventListener("click", function () {
    const chatSection = document.querySelector(".chat-section");
    chatSection.innerHTML = ""; // Clear chat section
});


// ... (previous script code) ...

document.addEventListener("DOMContentLoaded", function () {
    const newChatBox = document.getElementById("newChatBox");
    const chatMessages = document.querySelector(".chat-messages");
    const messageInput = document.getElementById("messageInput");
    const sendMessageButton = document.getElementById("sendMessageButton");
    const threeDots = document.getElementById("threeDots");
    const optionsBox = document.querySelector(".options-box");

    newChatBox.addEventListener("click", function () {
        chatMessages.innerHTML = "";
    });

    sendMessageButton.addEventListener("click", function () {
        // ... (previous send message code) ...
    });

    threeDots.addEventListener("click", function () {
        optionsBox.classList.toggle("show-options");
    });

    document.addEventListener("click", function (event) {
        if (!threeDots.contains(event.target) && !optionsBox.contains(event.target)) {
            optionsBox.classList.remove("show-options");
        }
    });
});
